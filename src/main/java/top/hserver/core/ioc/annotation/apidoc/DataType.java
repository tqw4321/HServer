package top.hserver.core.ioc.annotation.apidoc;

/**
 * @author hxm
 */

public enum DataType {
  String,
  Integer,
  Double,
  Boolean,
  File;
  private DataType() {
  }
}