package top.hserver.core.queue;

import lombok.Data;

/**
 * @author hxm
 */
@Data
public class QueueData {
    private Object[] args;
}
